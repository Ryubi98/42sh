#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <readline/history.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <fcntl.h>

#include "exec.h"
#include "command.h"
#include "operator.h"
#include "variables.h"
#include "utils.h"
#include "builtin.h"

static char **replace_variable(char **old_argv, struct variables **vs,
                                struct variables **alias)
{
    char **argv = copy_argv(old_argv, *alias);
    for (size_t i = 0; argv[i]; i++)
    {
        char *buffer = calloc(4096, sizeof(char));
        size_t j = 0;
        size_t k = 0;
        while (argv[i][k] && argv[i][k] != '$')
        {
            int quote = argv[i][k] == '\'';
            if (quote)
            {
                k++;
                while (argv[i][k] && argv[i][k] != '\'')
                    buffer[j++] = argv[i][k++];
                if (argv[i][k] && argv[i][k] == '\'')
                    k++;
            }
            if (argv[i][k] != '"' && !quote && argv[i][k] != '`')
                buffer[j++] = argv[i][k];
            k++;
        }
        while (argv[i][k] == '$' && argv[i][k] != '\0')
        {
            k++;
            const char *value = get_variable(*vs, argv[i] + k);
            while (*value)
            {
                buffer[j++] = *(value++);
                k++;
            }
        }
        free(argv[i]);
        argv[i] = realloc(buffer, strlen(buffer) + 1);
    }
    return argv;
}

static int get_out(struct ast *ast)
{
    if (ast->fdout_word == NULL)
        return 1;
    if (strcmp(ast->fdout_word, "0") == 0)
        return 0;
    if (strcmp(ast->fdout_word, "1") == 0)
        return 1;
    if (strcmp(ast->fdout_word, "2") == 0)
        return 2;
    int fd = 0;
    if (ast->redir_type == DOUBLE_RIGHT_CHEVRON)
        fd = open(ast->fdout_word, O_WRONLY | O_CREAT | O_APPEND, 0666);
    else
        fd = open(ast->fdout_word, O_WRONLY | O_CREAT | O_TRUNC, 0666);
    if (fd)
        return fd;
    return 0;
}

static int run_command(char *argv[], struct variables **vs, struct ast *ast)
{
    int fd_out = get_out(ast);
    int b = builtin(argv, vs, fd_out);
    if (b >= 0)
    {
        if (fd_out != 1)
            close(fd_out);
        return b;
    }
    pid_t pid = 0;
    int status;
    pid = fork();
    if (pid == 0)
    {
        if (fd_out != 1)
        {
            close(1);
            dup(fd_out);
        }
        if (execvp(argv[0], argv) == -1)
        {
            fprintf(stderr, "42sh: Error at execution: %s\n", argv[0]);
            exit(127);
        }
    }
    if (fd_out != 1)
        close(fd_out);
    waitpid(pid, &status, 0);
    if (WIFEXITED(status))
        return WEXITSTATUS(status);
    fprintf(stderr, "42sh: Error at execution: can't execute\n");
    return 126;
}

static int exec_list(struct ast *ast, struct variables **vs,
                        struct variables **alias)
{
    int status = 0;
    for (int i = 0; i < list_size(ast->list); i++)
    {
        status = execute(list_at(ast->list, i), vs, alias);
    }
    return status;
}

static int exec_cmd(struct ast *ast, struct variables **vs,
                    struct variables **alias)
{
    int status = 0;
    if (ast->argc > 0)
    {
        if (strcmp(ast->command[0], "unalias") == 0)
            return builtin_unalias(ast->command, alias);
        else if (strcmp(ast->command[0], "export") == 0)
            return builtin_export(ast->command, vs);
        else if (strcmp(ast->command[0], "alias") == 0)
            return builtin_alias(ast->command, alias);
    }
    char **argv = replace_variable(ast->command, vs, alias);
    status = run_command(argv, vs, ast);
    argv_free(argv);
    return status;
}

static int exec_assig(struct ast *ast, struct variables **vs,
    struct variables **alias)
{
    char **argv = replace_variable(ast->assignment, vs, alias);
    for (int i = 0; i < ast->arga; i++)
    {
        *vs = add_variable(*vs, argv[i], 0);
    }
    argv_free(argv);
    return 0;
}

int execute(struct ast *ast, struct variables **vs, struct variables **alias)
{
    if (ast == NULL)
        return 0;
    switch (ast->type)
    {
    case AST_LIST:
        return exec_list(ast, vs, alias);
    case AST_COMMAND:
        return exec_cmd(ast, vs, alias);
    case AST_IF:
        return exec_if(ast, vs, alias);
    case AST_WHILE:
        return exec_while(ast, vs, alias);
    case AST_FOR:
        return exec_for(ast, vs, alias);
    case AST_OP:
        return exec_op(ast, vs, alias);
    case AST_CASE:
        return exec_case(ast, vs, alias);
    case AST_UNTIL:
        return exec_until(ast, vs, alias);
    case AST_ASSIGNMENT:
        return exec_assig(ast, vs, alias);
    default:
        error("ast type unknown");
    }
    return 1;
}
