#ifndef EXEC_H
#define EXEC_H

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>

#include "ast.h"
#include "variables.h"

/**
  * \fn         int execute(struct ast *ast, struct variables **vs,
  *                         struct variables **alias)
  * \brief      Executes the ast.
  * \param ast  The ast to execute.
  * \return     Returns the status of the ast command.
  */

int execute(struct ast *ast, struct variables **vs, struct variables **alias);

/**
  * \fn         void error(char *str)
  * \brief      Generates an error.
  * \param str  The error to print.
  */

void error(char *str);

/**
  * \fn         void argv_free(char **argv)
  * \brief      Frees the argv.
  * \param argv The string corresponding of the argument.
  */

void argv_free(char **argv);

/**
  * \fn             char **copy_argv(char **argv, struct variables *alias)
  * \brief          Makes a copy of the current argv.
  * \param argv     The string corresponding of the argument to copy.
  * \param alias    A structure contening all the different aliases.
  * \return         Returns the copy of argv.
  */

char **copy_argv(char **argv, struct variables *alias);

#endif /* ! EXEC_H */
