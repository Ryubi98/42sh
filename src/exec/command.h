#ifndef COMMAND_H
#define COMMAND_H

/**
  * \fn             int exec_case(struct ast *ast, struct variables **vs,
  *                                 struct variables **alias)
  * \brief          Executes the case of the ast.
  * \param ast      The ast to execute.
  * \param vs       A structure contening all the different variables.
  * \param alias    A structure contening all the different aliases.
  * \return         Returns the status of the case.
  */

int exec_case(struct ast *ast, struct variables **vs,
                struct variables **alias);

/**
  * \fn             int exec_until(struct ast *ast, struct variables **vs,
  *                                 struct variables **alias)
  * \brief          Executes the until of the ast.
  * \param ast      The ast to execute.
  * \param vs       A structure contening all the different variables.
  * \param alias    A structure contening all the different aliases.
  * \return         Returns the status of the until.
  */

int exec_until(struct ast *ast, struct variables **vs,
                struct variables **alias);

/**
  * \fn             int exec_for(struct ast *ast, struct variables **vs,
  *                                 struct variables **alias)
  * \brief          Executes the for of the ast.
  * \param ast      The ast to execute.
  * \param vs       A structure contening all the different variables.
  * \param alias    A structure contening all the different aliases.
  * \return         Returns the status of the for.
  */

int exec_for(struct ast *ast, struct variables **vs,
                struct variables **alias);


/**
  * \fn             int exec_if(struct ast *ast, struct variables **vs,
  *                                 struct variables **alias)
  * \brief          Executes the if of the ast.
  * \param ast      The ast to execute.
  * \param vs       A structure contening all the different variables.
  * \param alias    A structure contening all the different aliases.
  * \return         Returns the status of the if.
  */

int exec_if(struct ast *ast, struct variables **vs,
            struct variables **alias);

/**
  * \fn             int exec_while(struct ast *ast, struct variables **vs,
  *                                 struct variables **alias)
  * \brief          Executes the while of the ast.
  * \param ast      The ast to execute.
  * \param vs       A structure contening all the different variables.
  * \param alias    A structure contening all the different aliases.
  * \return         Returns the status of the while.
  */

int exec_while(struct ast *ast, struct variables **vs,
                struct variables **alias);

#endif /* ! COMMAND_H */
