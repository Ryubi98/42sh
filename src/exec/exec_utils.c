#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <readline/history.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#include "exec.h"
#include "command.h"
#include "operator.h"
#include "variables.h"
#include "utils.h"
#include "builtin.h"

void error(char *str)
{
    fprintf(stderr, "42sh: Error while executing: %s\n", str);
}

void argv_free(char **argv)
{
    int i = 0;
    while (argv[i])
        free(argv[i++]);
    free(argv);
}

char **copy_argv(char **argv, struct variables *alias)
{
    int argc = argvlen(argv);
    char **cpy = calloc(argc + 1, sizeof(char *));
    for (int i = 0; i < argc; i++)
    {
        int a = 0;
        if (alias != NULL)
        {
            for (size_t j = 0; j < alias->size; j++)
            {
                if (strcmp(alias->array[j]->name, argv[i]) == 0)
                {
                    a = 1;
                    cpy[i] = calloc(4096, sizeof(char));
                    cpy[i] = strcpy(cpy[i], alias->array[j]->value);
                    break;
                }
            }
        }
        if (a == 0)
        {
            cpy[i] = calloc(4096, sizeof(char));
            cpy[i] = strcpy(cpy[i], argv[i]);
        }
    }
    return cpy;
}