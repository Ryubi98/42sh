#ifndef OPERATOR_H
#define OPERATOR_H

/**
  * \fn             int exec_not(struct ast *a, struct ast *b,
  *                             struct variables **vs,
  *                             struct variables **alias)
  * \brief          Executes the negation.
  * \param a        The first argument to check.
  * \param b        The second argument to check.
  * \param vs       A structure contening all the different variables.
  * \param alias    A structure contenin all the diffrent aliases.
  * \return         Returns the status of the negation.
  */

int exec_not(struct ast *a, struct ast *b, struct variables **vs,
                struct variables **alias);

/**
  * \fn             int exec_or(struct ast *a, struct ast *b,
  *                             struct variables **vs,
  *                             struct variables **alias)
  * \brief          Executes the OR.
  * \param a        The first argument to check.
  * \param b        The second argument to check.
  * \param vs       A structure contening all the different variables.
  * \param alias    A structure contenin all the diffrent aliases.
  * \return         Returns the status of the OR.
  */

int exec_or(struct ast *a, struct ast *b, struct variables **vs,
            struct variables **alias);

/**
  * \fn             int exec_and(struct ast *a, struct ast *b,
  *                                 struct variables **vs,
  *                                 struct variables **alias)
  * \brief          Executes the AND.
  * \param a        The first argument to check.
  * \param b        The second argument to check.
  * \param vs       A structure contening all the different variables.
  * \param alias    A structure contenin all the diffrent aliases.
  * \return         Returns the status of the AND.
  */

int exec_and(struct ast *a, struct ast *b, struct variables **vs,
                struct variables **alias);

/**
  * \fn             int exec_not(struct ast *a, struct ast *b,
  *                                 struct variables **vs,
  *                                 struct variables **alias)
  * \brief          Handles all the operators.
  * \param a        The first argument to check.
  * \param b        The second argument to check.
  * \param vs       A structure contening all the different variables.
  * \param alias    A structure contenin all the diffrent aliases.
  * \return         Returns the status of the operator.
  */

int exec_op(struct ast *ast, struct variables **vs,
            struct variables **alias);

#endif /* ! OPERATOR_H */
