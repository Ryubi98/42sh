#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <readline/history.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#include "exec.h"
#include "command.h"
#include "operator.h"
#include "variables.h"
#include "utils.h"
#include "builtin.h"

static void make_pipe(char *out[], char *in[])
{
    int pdes[2] =
    {
        0
    };
    pipe(pdes);
    if (fork() == 0)
    {
        close(1);
        dup(pdes[1]);
        close(pdes[0]);
        close(pdes[1]);
        if (execvp(out[0], out) == -1)
        {
            fprintf(stderr, "42sh: Error at execution: can't execute\n");
            exit(EXIT_FAILURE);
        }
        exit(EXIT_SUCCESS);
    }
    else
    {
        close(0);
        dup(pdes[0]);
        close(pdes[0]);
        close(pdes[1]);
        if (execvp(in[0], in) == -1)
        {
            fprintf(stderr, "42sh: Error at execution: can't execute\n");
            exit(EXIT_FAILURE);
        }
        exit(EXIT_SUCCESS);
    }
    exit(EXIT_FAILURE);
}

static int exec_pipe(struct ast *a, struct ast *b)
{
    if (a->type != AST_COMMAND || b->type != AST_COMMAND)
        return 0;
    pid_t pid = 0;
    int status;
    pid = fork();
    if (pid == 0)
    {
        make_pipe(a->command, b->command);
    }
    waitpid(pid, &status, 0);
    if (WIFEXITED(status))
        return WEXITSTATUS(status);
    fprintf(stderr, "42sh: Error at execution: can't execute\n");
    return 1;
}

int exec_not(struct ast *a, struct ast *b, struct variables **vs,
                    struct variables **alias)
{
    if (b != NULL)
        error("! operator takes only one argument");
    if (execute(a, vs, alias) == 0)
        return 1;
    return 0;
}

int exec_or(struct ast *a, struct ast *b, struct variables **vs,
                    struct variables **alias)
{
    if (execute(a, vs, alias) != 0)
        return execute(b, vs, alias);
    return 0;
}

int exec_and(struct ast *a, struct ast *b, struct variables **vs,
                    struct variables **alias)
{
    if (execute(a, vs, alias) == 0)
        return execute(b, vs, alias);
    return 1;
}

int exec_op(struct ast *ast, struct variables **vs,
                    struct variables **alias)
{
    if (ast->op == OR)
        return exec_or(ast->body, ast->else_body, vs, alias);
    if (ast->op == AND)
        return exec_and(ast->body, ast->else_body, vs, alias);
    if (ast->op == PIPE)
        return exec_pipe(ast->body, ast->else_body);
    if (ast->op == NOT)
        return exec_not(ast->body, ast->else_body, vs, alias);
    if (ast->op == EQ)
    {
        int exec = execute(ast->else_body, vs, alias);
        return !(execute(ast->body, vs, alias) == exec);
    }
    if (ast->op == GT)
    {
        int exec = execute(ast->else_body, vs, alias);
        return !(execute(ast->body, vs, alias) > exec);
    }
    if (ast->op == GTE)
    {
        int exec = execute(ast->else_body, vs, alias);
        return !(execute(ast->body, vs, alias) >= exec);
    }
    if (ast->op == LT)
    {
        int exec = execute(ast->else_body, vs, alias);
        return !(execute(ast->body, vs, alias) < exec);
    }
    if (ast->op == LTE)
    {
        int exec = execute(ast->else_body, vs, alias);
        return !(execute(ast->body, vs, alias) <= exec);
    }
    return 0;
}