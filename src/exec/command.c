#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <readline/history.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#include "exec.h"
#include "command.h"
#include "operator.h"
#include "variables.h"
#include "utils.h"
#include "builtin.h"

int exec_case(struct ast *ast, struct variables **vs,
                        struct variables **alias)
{
    int status = 0;
    while (execute(ast->condition, vs, alias) == 0)
    {
        status = execute(ast->body, vs, alias);
    }
    return status;
}

int exec_until(struct ast *ast, struct variables **vs,
                        struct variables **alias)
{
    int status = 0;
    while (execute(ast->condition, vs, alias) != 0)
    {
        status = execute(ast->body, vs, alias);
    }
    return status;
}

int exec_for(struct ast *ast, struct variables **vs,
                    struct variables **alias)
{
    int status = 0;
    for (int i = 0; i < ast->argf; i++)
    {
        *vs = f_add_variable(*vs, ast->var_name, ast->for_list[i]);
        status = execute(ast->body, vs, alias);
    }
    return status;
}

int exec_if(struct ast *ast, struct variables **vs,
                    struct variables **alias)
{
    int status = 0;
    int condition = execute(ast->condition, vs, alias);
    if (condition == 0)
        status = execute(ast->body, vs, alias);
    else
        status = execute(ast->else_body, vs, alias);
    return status;
}

int exec_while(struct ast *ast, struct variables **vs,
                        struct variables **alias)
{
    int status = 0;
    while (execute(ast->condition, vs, alias) == 0)
    {
        status = execute(ast->body, vs, alias);
    }
    return status;
}