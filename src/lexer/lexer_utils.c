#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string.h>

#include <unistd.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "lexer.h"
#include "lexer_utils.h"
#include "utils.h"
#include "variables.h"

int is_doublon(struct lexer *lexer, int type, int len)
{
    char c = *(lexer->buffer + lexer->start + len);
    switch (c)
    {
    case ' ':
    case '\n':
    case '\t':
    case '\0':
        return 0;
    case '&':
        return type == 2;
    case '|':
        return type == 3;
    case ';':
        return type == 4;
    case '<':
        return type == 5;
    case '>':
        return type == 6;
    default:
        return 0;
    }
}

int is_separator_word(struct lexer *lexer, int i)
{
    char c = *(lexer->buffer + lexer->start + i);
    switch (c)
    {
    case ' ':
    case '\n':
    case '\t':
    case '\0':
    case ';':
    case '&':
    case '|':
        return 1;
    default:
        return 0;
    }
}

int is_quote(char c, char *end_char)
{
    switch (c)
    {
    case '"':
    case '\'':
    case '`':
        *end_char = c;
        return 1;
    default:
        return 0;
    }
}

int is_assignment_word(const char *str)
{
    if (str == NULL || *str == '\0' || *str == '=')
        return 0;
    str++;
    while (*str != '\0')
    {
        if (*str == '=')
            return 1;
        str++;
    }
    return 0;
}

static int get_keyword_check(struct lexer *lexer, enum TOKEN type,
    const char *keyword)
{
    size_t len = strlen(keyword);
    if (strncmp(lexer->buffer + lexer->start, keyword, len) == 0
        && ! is_doublon(lexer, type, len))
        return len;
    return 0;
}

int get_keyword(struct lexer *lexer, enum TOKEN type)
{
    const char *dico_types[] =
    {
        "&&",
        "||",
        "&",
        "|",
        ";",
        "<",
        ">",
        "<<",
        ">>",
        "if",
        "elif",
        "fi",
        "then",
        "else",
        "while",
        "until",
        "for",
        "do",
        "done",
        "case",
        "esac",
        "in",
        "!",
        "\n"
    };
    return get_keyword_check(lexer, type, dico_types[type]);
}