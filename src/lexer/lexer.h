#ifndef LEXER_H
#define LEXER_H

#include "options.h"
#include <sys/types.h>

#define BUFF_SIZE 4096

/**
* \enum     TOKEN
* \brief    Enumeration corresponding to a shell keyword.
*/

enum TOKEN
{
    LOGICAL_AND,
    LOGICAL_OR,
    BITWISE_AND,
    BITWISE_OR,
    SEMICOLON,
    LEFT_CHEVRON,
    RIGHT_CHEVRON,
    DOUBLE_LEFT_CHEVRON,
    DOUBLE_RIGHT_CHEVRON,
    IF,
    ELIF,
    FI,
    THEN,
    ELSE,
    WHILE,
    UNTIL,
    FOR,
    DO,
    DONE,
    CASE,
    ESAC,
    IN,
    EM,
    NEW_LINE,
    END_OF_FILE,
    WORD,
    ASSIGNMENT_WORD
};

/**
* \enum     INPUT_TYPE
* \brief    Enumeration corresponding to the different intput type.
*/

enum INPUT_TYPE
{
    ARG,
    INPUT,
    INTERACTIVE,
    SCRIPT
};

/**
* \struct lexer
* \brief  Structure corresponding to the lexer.
*/

struct lexer
{
    enum INPUT_TYPE mode;
    ssize_t start;
    ssize_t end;
    char *buffer;
    size_t size;
    FILE *script_file;

    char *word;
    int error;
    char *error_msg;
    struct variables **vs;
};

/**
* \fn               struct lexer *lexer_init(char *c, struct options *options,
*                                               struct variables **vs)
* \brief            Initializes a structure lexer.
* \param c          The current caracter.
* \param options    A structure contening all the different options.
* \param vs         A structure contening all the different variables.
* \return           Returns the new lexer.
*/

struct lexer *lexer_init(char *c, struct options *options,
                            struct variables **vs);

/**
* \fn               int lexer_peek(struct lexer *lexer, enum TOKEN type,
*                                   int needed)
* \brief            Checks the type of the current argument.
* \param lexer      The current lexer.
* \param type       The type to check.
* \param need       Integer that indicates if the element is needed or not.
* \return           Return 1 if the current argument is the good type,
*                   otherwhise 0.
*/

int lexer_peek(struct lexer *lexer, enum TOKEN type, int needed);

/**
* \fn           int lexer_pop(struct lexer *lexer, enum TOKEN type, int needed)
* \brief        Removes and checks the current argument.
* \param lexer  The current lexer.
* \param type   The type to check.
* \param need   Integer that indicates if the element is needed or not.
* \return       Return 1 if the current argument is of the good type,
*               otherwhise 0.
*/

int lexer_pop(struct lexer *lexer, enum TOKEN type, int needed);

/**
* \fn           int update_buffer(struct lexer *lexer, int needed)
* \brief        Updates the current buffer.
* \param lexer  The lexer to update.
* \param need   Tells if the current element is needed or not.
* \return       Returns 1 if the line is ended, otherwhise 0.
*/

int update_buffer(struct lexer *lexer, int needed);

/**
<<<<<<< HEAD
* \fn int get_word(struct lexer *lexer, int assignment)
* \brief            explain
* \param lexer      explain
* \param assignment explain
*/

int get_word(struct lexer *lexer, int assignment);

/**
* \fn           void lexer_free(struct lexer *lexer)
* \brief        Frees the lexer structure.
* \param lexer  The lexer to free.
*/

void lexer_free(struct lexer *lexer);

#endif /* ! LEXER_H */