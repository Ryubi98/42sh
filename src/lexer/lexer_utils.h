#ifndef LEXER_UTILS_H
#define LEXER_UTILS_H

/**
* \fn           int is_doublon(struct lexer *lexer, int type, int len)
* \brief        Checks if there is doublon.
* \param lexer  The current lexer.
* \param types  The type of the element.
* \param len    The lenght of the element.
* \return       Returns 1 if there is a doublon, otherwhise 0.
*/

int is_doublon(struct lexer *lexer, int type, int len);

/**
* \fn           int is_separator_word(struct lexer *lexer, int i)
* \brief        Checks if the current element is a separator word.
* \param lexer  The current lexer.
* \param i      The index in the lexer.
* \return       Returns 1 if the element is a separator word, otherwhise 0.
*/

int is_separator_word(struct lexer *lexer, int i);

/**
* \fn               int is_quote(char c, char *end_char)
* \brief            Checks if the current caracter is a quote.
* \param c          The current caracter.
* \param end_char   A pointer to get the current quote if there is one.
* \return           Returns 1 if the current caracter is a quote, otherwhise 0.
*/

int is_quote(char c, char *end_char);

/**
* \fn           int is_assignment_word(const char *str)
* \brief        Checks if the current element is an assignment word.
* \param str    The current word.
* \return       Returns 1 if the word is an assignment word, otherwhise 0.
*/

int is_assignment_word(const char *str);

/**
* \fn           int get_keyword(struct lexer *lexer, enum TOKEN type)
* \brief        Gets the current keyword.
* \param lexer  The current lexer.
* \param type   The type of the keyword.
* \return       Returns 1 if the keyword is valid, otherwhise 0.
*/

int get_keyword(struct lexer *lexer, enum TOKEN type);

#endif /* ! LEXER_UTILS_H */