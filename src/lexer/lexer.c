#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string.h>

#include <unistd.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "lexer.h"
#include "lexer_utils.h"
#include "utils.h"
#include "variables.h"

struct lexer *lexer_init(char *c, struct options *options,
                            struct variables **vs)
{
    struct lexer *lexer = calloc(1, sizeof(struct lexer));
    lexer->size = BUFF_SIZE;
    lexer->vs = vs;
    lexer->buffer = calloc(BUFF_SIZE, sizeof(char));
    if (c == NULL)
    {
        if (isatty(STDIN_FILENO) && options->script == NULL)
        {
            lexer->mode = INTERACTIVE;
            char *buff = readline(get_variable(*vs, "PS1"));
            add_history(buff);
            lexer->end = snprintf(lexer->buffer, 4096, "%s", buff);
            free(buff);
        }
        else
        {
            FILE *fd = stdin;
            lexer->mode = INPUT;
            if (options->script != NULL)
            {
                lexer->mode = SCRIPT;
                fd = options->script;
                lexer->script_file = options->script;
            }
            do
            {
                lexer->end = getline(&lexer->buffer, &lexer->size, fd);
            } while (lexer->buffer[0] == '\n' && lexer->end != -1);
        }
    }
    else
        lexer->end = snprintf(lexer->buffer, 4096, "%s", c);
    return lexer;
}

static int pull_line(struct lexer *lexer, int needed)
{
    if (lexer->mode == INTERACTIVE)
    {
        if (!needed)
            return 0;
        char *buff = readline(get_variable(*(lexer->vs), "PS2"));
        for (int i = 0; i < 4096; i++)
            *(lexer->buffer + i) = 0;
        lexer->end = snprintf(lexer->buffer, 4096, "%s", buff);
        lexer->start = 0;
        add_history_with_previous(buff);
    }
    else
    {
        lexer->size = 4096;
        if (lexer->mode == SCRIPT)
            lexer->end = getline(&lexer->buffer, &lexer->size,
                lexer->script_file);
        else
            lexer->end = getline(&lexer->buffer, &lexer->size, stdin);
        if (lexer->end >= 0)
            lexer->start = 0;
        else
            return 0;
    }
    return 1;
}

int update_buffer(struct lexer *lexer, int needed)
{
    int line_pulled = 1;
    while (line_pulled)
    {
        while (*(lexer->buffer + lexer->start) == ' '
                || *(lexer->buffer + lexer->start) == '\t')
            lexer->start += 1;

        if (*(lexer->buffer + lexer->start) == '#')
            lexer->start = lexer->end;

        if (lexer->start < lexer->end)
            return 1;

        line_pulled = 0;
        if (lexer->mode != ARG)
            line_pulled = pull_line(lexer, needed);
        else
            return 0;
    }
    return 0;
}

static int get_word_iteration(struct lexer *lexer)
{
    int i = 0;
    int j = 0;
    char c = *(lexer->buffer + lexer->start);
    while (lexer->start + i < 4096)
    {
        if (is_separator_word(lexer, i))
            break;
        char end_quote = '\0';
        if (is_quote(c, &end_quote))
        {
            *(lexer->word + j++) = c;
            c = *(lexer->buffer + lexer->start + ++i);
            while (c != '\0' && c != end_quote)
            {
                *(lexer->word + j++) = c;
                c = *(lexer->buffer + lexer->start + ++i);
                if (lexer->mode != ARG && c != end_quote
                    && (c == '\n' || c == '\0'))
                {
                    lexer->start += i;
                    if (!pull_line(lexer, 1))
                        break;
                    i = 0;
                    *(lexer->word + j++) = '\n';
                    c = *(lexer->buffer + lexer->start + i);
                }
            }
        }
        *(lexer->word + j++) = c;
        c = *(lexer->buffer + lexer->start + ++i);
    }

    return i;
}

int get_word(struct lexer *lexer, int assignment)
{
    if (lexer->buffer[0] == '#')
        return 0;
    lexer->word = calloc(4096, sizeof(char));
    
    int i = get_word_iteration(lexer);

    if (assignment && !is_assignment_word(lexer->word))
    {
        free(lexer->word);
        lexer->word = NULL;
        return 0;
    }
    int size = strlen(lexer->word);
    lexer->word = realloc(lexer->word, size + 1);
    if (size == 0)
    {
        free(lexer->word);
        lexer->word = NULL;
        return 0;
    }
    return i;
}

static int lexer_get(struct lexer *lexer, enum TOKEN type, int pop, int needed)
{
    if (!update_buffer(lexer, needed))
        return type == END_OF_FILE;
    if (type == END_OF_FILE)
        return 0;
    int shift = 0;
    if (type == WORD)
        shift = get_word(lexer, 0);
    else if (type == ASSIGNMENT_WORD)
        shift = get_word(lexer, 1);
    else
        shift = get_keyword(lexer, type);
    if (pop)
    {
        if (lexer->end < 0)
            return 0;
        lexer->start += shift;
        //printf("lexer buffer |%s|\n", lexer->buffer + lexer->start);
    }
    return shift > 0;
}

int lexer_peek(struct lexer *lexer, enum TOKEN type, int needed)
{
    return lexer_get(lexer, type, 0, needed);
}

int lexer_pop(struct lexer *lexer, enum TOKEN type, int needed)
{
    return lexer_get(lexer, type, 1, needed);
}

void lexer_free(struct lexer *lexer)
{
    free(lexer->error_msg);
    free(lexer->buffer);
    free(lexer);
}