#ifndef UTILS_H
#define UTILS_H

/**
  * \fn      char *get_path_history(void)
  * \brief   Get the path of the home directory
  * \return  Return a string corresponding of the home path directory.
  */

char *get_path_history(void);

/**
  * \fn          void add_history_with_previous(char *buff)
  * \brief       Concatenate history on multiple lines.
  * \param buff  It is the previous command.
  */

void add_history_with_previous(char *buff);

#endif /* ! UTILS_H */
