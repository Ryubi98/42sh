#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <sys/stat.h>

#include "utils.h"

char *get_path_history(void)
{
    char *home_dir = secure_getenv("HOME");
    if (home_dir == NULL)
        return NULL;

    const char *name = "/.42sh_history";
    size_t len = strlen(home_dir) + strlen(name) + 1;
    char *home = calloc(len, sizeof(char));
    if (home == NULL)
        return NULL;

    size_t i = 0;
    for (size_t j = 0; j < strlen(home_dir); j++, i++)
        home[i] = home_dir[j];
    for (size_t j = 0; j < strlen(name); j++, i++)
        home[i] = name[j];
    return home;
}

void add_history_with_previous(char *buff)
{
    HISTORY_STATE *state = history_get_history_state();
    HIST_ENTRY *last = remove_history(state->length - 1);

    size_t new_size = strlen(buff) + strlen(last->line) + 2;
    char *new = calloc(new_size, sizeof(char));

    new = strcat(new, last->line);
    new = strcat(new, " ");
    new = strcat(new, buff);

    add_history(new);

    free(new);
    free(last->line);
    free(last->timestamp);
    free(last->data);
    free(last);
    free(buff);
    free(state);
}
