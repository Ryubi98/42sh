#define GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>

#include "variables.h"

void remove_all_variables(struct variables *alias)
{
    if (alias != NULL)
    {
        for (size_t i = 0; i < alias->size; i++)
        {
            free(alias->array[i]->name);
            free(alias->array[i]);
        }
        alias->size = 0;
    }
}

static void swap_variables(struct variables *alias, int i)
{
    free(alias->array[i]->name);
    free(alias->array[i]);
    alias->array[i] = alias->array[alias->size - 1];
    alias->size -= 1;
}

int remove_variable(struct variables *alias, const char *name)
{
    if (alias != NULL)
    {
        if (alias->size <= (alias->capacity / 2))
        {
            alias->capacity /= 2;
            alias->array = realloc(alias->array,
                                alias->capacity * sizeof(struct variable *));
        }
        for (size_t i = 0; i < alias->size; i++)
        {
            struct variable *var = alias->array[i];
            if (strcmp(var->name, name) == 0)
            {
                swap_variables(alias, i);
                return 0;
            }
        }
    }
    fprintf(stderr, "42sh: unalias: %s: not found\n", name); 
    return 1; 
}