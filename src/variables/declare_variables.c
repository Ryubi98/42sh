#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <readline/history.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#include "exec.h"
#include "variables.h"

void declare_ps1_ps2(struct variables **vs)
{
    *vs = add_variable(*vs, "PS1=42sh$ ", 0);
    *vs = add_variable(*vs, "PS2=> ", 0);
}

void declare_pwd(struct variables **vs)
{
    char *pwd = calloc(4096, sizeof(char));
    snprintf(pwd, 5, "PWD=");
    getcwd(pwd + 4, 4096 - 4);
    *vs = add_variable(*vs, pwd, 0);
    free(pwd);

    char *oldpwd = calloc(4096, sizeof(char));
    snprintf(oldpwd, 8, "OLDPWD=");
    getcwd(oldpwd + 7, 4096 - 7);
    *vs = add_variable(*vs, oldpwd, 0);
    free(oldpwd);
}

void declare_status(struct variables **vs, int status)
{
    char *stat_str = calloc(32, sizeof(char));
    snprintf(stat_str, 32, "?=%d", status);
    *vs = add_variable(*vs, stat_str, 0);
    free(stat_str);
}