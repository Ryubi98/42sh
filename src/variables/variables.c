#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>

#include "variables.h"

static int var_len(const char *s)
{
    int i = 0;
    while (*s && *s != '$' && *s != '"')
    {
        s++;
        i++;
    }
    return i;
}

static struct variables *init_variable(struct variables *vs,
                                        char *n, char *v, int ex)
{
    struct variable *variable = calloc(1, sizeof(struct variable));
    variable->name = n;
    variable->value = v;
    variable->is_export = ex;
    vs->array[vs->size] = variable;
    vs->size += 1;
    return vs;
}

struct variables *add_variable(struct variables *vs, char *assignment, int ex)
{
    if (vs == NULL)
    {
        vs = calloc(1, sizeof(struct variables));
        vs->array = malloc(10 * sizeof(struct variable *));
        vs->capacity = 10;
    }
    char *value = calloc(4096, sizeof(char));
    snprintf(value, 4096, "%s", assignment);
    value = realloc(value, strlen(value) + 1);
    char *name = strsep(&value, "=");
    if (vs->size == vs->capacity)
    {
        vs->capacity *= 2;
        vs->array = realloc(vs->array,
                            vs->capacity * sizeof(struct variable *));
    }
    for (size_t i = 0; i < vs->size; i++)
    {
        struct variable *var = vs->array[i];
        if (strcmp(var->name, name) == 0)
        {
            free(var->name);
            var->name = name;
            var->value = value;
            var->is_export = var->is_export || ex;
            return vs;
        }
    }
    return init_variable(vs, name, value, ex);
}

struct variables *f_add_variable(struct variables *vs, const char *n,
                                    const char *v)
{
    if (vs == NULL)
    {
        vs = calloc(1, sizeof(struct variables));
        vs->array = malloc(10 * sizeof(struct variable *));
        vs->capacity = 10;
    }
    char *value = calloc(4096, sizeof(char));
    snprintf(value, 4096, "%s=%s", n, v);
    value = realloc(value, strlen(value) + 1);
    char *name = strsep(&value, "=");
    if (vs->size == vs->capacity)
    {
        vs->capacity *= 2;
        vs->array = realloc(vs->array,
                            vs->capacity * sizeof(struct variable *));
    }
    for (size_t i = 0; i < vs->size; i++)
    {
        struct variable *var = vs->array[i];
        if (strcmp(var->name, name) == 0)
        {
            free(var->name);
            var->name = name;
            var->value = value;
            return vs;
        }
    }
    return init_variable(vs, name, value, 0);
}

const char *get_variable(struct variables *vs, const char *name)
{
    if (vs == NULL)
    {
        return NULL;
    }
    int len = var_len(name);
    if (len == 0 || name == NULL)
    {
        return "";
    }
    for (size_t i = 0; i < vs->size; i++)
    {
        struct variable *var = vs->array[i];
        if (var->name != NULL && strncmp(var->name, name, len) == 0)
            return var->value;
    }
    return "";
}

void free_variables(struct variables *vs)
{
    if (vs == NULL)
        return;
    for (size_t i = 0; i < vs->size; i++)
    {
        free(vs->array[i]->name);
        free(vs->array[i]);
    }
    free(vs->array);
    free(vs);
}

void dump_variables(const char *s, struct variables *vs)
{
    if (vs == NULL)
        return;
    for (size_t i = 0; i < vs->size; i++)
    {
        if (vs->array[i]->is_export)
        {
            printf("%s %s=\"%s\"\n", s,
                vs->array[i]->name, vs->array[i]->value);
        }
    }
}
