#ifndef VARIABLES_H
#define VARIABLES_H

#include <stdlib.h>
#include <string.h>

/**
 * \struct variable
 * \brief  Structure corresponding to one variable.
 */

struct variable
{
    char *name;
    char *value;
    int is_export;
};

/**
 * \struct variables
 * \brief  Structure corresponding to an array of variables.
 */

struct variables
{
    size_t capacity;
    size_t size;
    struct variable **array;
};

/**
  * \fn                 struct variables *add_variable(struct variables *vs,
  *                                                     char *assignment,
  *                                                     int ex)
  * \brief              Adds a variable in the structure.
  * \param vs           Structure containing all the variables.
  * \param assignment   It is the string of the assignment variable.
  * \param ex           Indicator for the export builtin.
  * \return             Returns the structure with all the variables.
  */

struct variables *add_variable(struct variables *vs, char *assignment, int ex);

/**
  * \fn             struct variables *f_add_variable(struct variables *vs,
  *                                                     const char *name,
                                                        const char *value)
  * \brief          Adds a variable in the structure with the name and value.
  * \param vs       Structure containing all the variables.
  * \param name     It is the name of the variable.
  * \param value    It is the value of the variable.
  * \return         Returns the structure with all the variables.
  */

struct variables *f_add_variable(struct variables *vs, const char *name,
                                    const char *value);

/**
  * \fn         const char *get_variable(struct variables *vs, const char *name)
  * \brief      Gives the value of a variable.
  * \param vs   Structure containing all the variables.
  * \param name Name of the variable.
  * \return     Returns the value of the variable if it exists.
  */

const char *get_variable(struct variables *vs, const char *name);

/**
  * \fn         int remove_variable(struct variables *vs, const char *name)
  * \brief      Removes a variable from the structure.
  * \param vs   Structure containing all the variables.
  * \param name Name of the variable.
  * \return     Returns 0 on success, otherwish 1.
  */

int remove_variable(struct variables *vs, const char *name);

/**
  * \fn          void remove_all_variables(struct variables *alias)
  * \brief       Removes every variables from the structure.
  * \param alias Structure containing all the variables.
  */

void remove_all_variables(struct variables *alias);

/**
  * \fn        void free_variables(struct variables *vs)
  * \brief     Frees every variables and the structure.
  * \param vs  Structure containing all the variables.
  */

void free_variables(struct variables *vs);

/**
  * \fn       void dump_variables(const char *s, struct variables *vs)
  * \brief    Displays a variable.
  * \param s  Name of the variable to display.
  * \param vs Struccture containing all the variables.
  */

void dump_variables(const char *s, struct variables *vs);

/**
  * \fn       void declare_ps1_ps2(struct variables **vs)
  * \brief    Declares the variables PS1 and PS2.
  * \param vs Structure containing all the variables.
  */

void declare_ps1_ps2(struct variables **vs);

/**
  * \fn       void declare_pwd(struct variables **vs);
  * \brief    Declares the variable PWD for the current directory.
  * \param vs Structure containing all the variables.
  */

void declare_pwd(struct variables **vs);

/**
  * \fn           void declare_status(struct variables **vs, int status)
  * \brief        Declares the status of a variable.
  * \param vs     Structure containing all the variables.
  * \param status Status of the variable.
  */

void declare_status(struct variables **vs, int status);

#endif /* ! VARIABLES_H */