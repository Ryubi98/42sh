#ifndef PARSER_H
#define PARSER_H

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>
#include <string.h>

#include "ast.h"
#include "lexer.h"

/**
  * \fn struct ast *parser(struct lexer *lexer)
  * \brief            parse the input
  * \param lexer      lexer which permit to lex interactively
  * \return           the ast
  */

struct ast *parser(struct lexer *lexer);

/**
  * \fn struct ast *parser_input(struct lexer *lexer)
  * \brief            start of parsing rule grammar
  * \param lexer      lexer which permit to lex interactively
  * \return           the ast
  */

struct ast *parser_input(struct lexer *lexer);

/**
  * \fn struct ast *parser_list(struct lexer *lexer)
  * \brief            parse the rule list of the grammar
  * \param lexer      lexer which permit to lex interactively
  * \return           the ast
  */

struct ast *parser_list(struct lexer *lexer);

/**
  * \fn struct ast *parser_and_or(struct lexer *lexer)
  * \brief            parse the rule and_or of the grammar
  * \param lexer      lexer which permit to lex interactively
  * \return           the ast
  */

struct ast *parser_and_or(struct lexer *lexer, int needed);

/**
  * \fn struct ast *parser_pipeline(struct lexer *lexer, int needed)
  * \brief            parse the rule pipeline of the grammar
  * \param lexer      lexer which permits to lex interactively
  * \param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

struct ast *parser_pipeline(struct lexer *lexer, int needed);

/**
  * \fn struct ast *parser_command(struct lexer *lexer, int needed)
  * \brief            parse the rule command of the gramar
  * \param lexer      lexer which permits to lex interactively
  * \param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

struct ast *parser_command(struct lexer *lexer, int needed);

/**
  * \fn struct ast *parser_simple_command(struct lexer *lexer, int needed)
  * \brief            parse the rule simple_command of the grammar
  * \param lexer      lexer which permits to lex interactively
  * \param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

struct ast *parser_simple_command(struct lexer *lexer, int needed);

/**
  * \fn struct ast *parser_shell_command(struct lexer *lexer, int needed)
  * \brief            parse the rule shell_command of the grammar
  * \param lexer      lexer which  permits to lex interactively
  * \param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

struct ast *parser_shell_command(struct lexer *lexer, int needed);

/**
  * \fn struct ast *parser_funcdec(struct lexer *lexer, int needed)
  * \brief            parse the rule funcded of the grammar
  * \param lexer      lexer which permits to lex interactively
  * \param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

struct ast *parser_funcdec(struct lexer *lexer, int needed);

/**
  * \fn struct ast *parser_redirection(struct lexer *lexer, int needed)
  * \brief            parse the rule redirection of the grammar
  * \param lexer      lexer which permits to lex interactively
  * \param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

int parser_redirection(struct lexer *lexer, int needed, struct ast *ast);

/**
  * \fn struct ast *parser_prefix(struct lexer *lexer, int needed)
  * \brief            parse the rule prefix of the grammar
  * \param lexer      lexer which permits to lex interactively
  * 'param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

int parser_prefix(struct lexer *lexer, int needed, struct ast *ast);

/**
  * \fn struct ast *parser_element(struct lexer *lexer, int needed)
  * \brief            parse the rule element of the grammar
  * \param lexer      lexer which permits to lex interactively
  * \param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

int parser_element(struct lexer *lexer, int needed, struct ast *ast);

/**
  * \fn struct ast *parser_compound_list(struct lexer *lexer, int needed)
  * \brief            parse the rule coumpound_list of the grammar
  * \param lexer      lexer permits to lex interactively
  * \param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

struct ast *parser_compound_list(struct lexer *lexer, int needed);

/**
  * \fn struct ast *parser_rule_for(struct lexer *lexer, int needed)
  * \brief            parse the rule for of the grammar
  * \param lexer      lexer which permits to lex interactively
  * \param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

struct ast *parser_rule_for(struct lexer *lexer, int needed);

/**
  * \fn struct ast *parser_rule_while(struct lexer *lexer, int needed)
  * \brief            parse the rule while of the grammar
  * \param lexer      lexer which permits to lex interactively
  * 'param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

struct ast *parser_rule_while(struct lexer *lexer, int needed);

/**
  * \fn struct ast *parser_rule_case(struct lexer *lexer, int needed)
  * \brief            parse the rule case of the grammar
  * \param lexer      lexer which permits to lex interactively
  * \param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

struct ast *parser_rule_case(struct lexer *lexer, int needed);

/**
  * \fn struct ast *parser_rule_until(struct lexer *lexer, int needed)
  * \brief            parse the rule until of the grammar
  * \param lexer      lexer which permits to lex interactively
  * \param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

struct ast *parser_rule_until(struct lexer *lexer, int needed);

/**
  * \fn struct ast *parser_rule_if(struct lexer *lexer, int needed)
  * \brief            parse the rule if of the grammar
  * \param lexer      lexer which permits to lex interactively
  * \param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

struct ast *parser_rule_if(struct lexer *lexer, int needed);

/**
  * \fn struct ast *parser_else_clause(struct lexer *lexer)
  * \brief            parse the rule else of the grammar
  * \param lexer      lexer which permits to lex interactively
  * \param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

struct ast *parser_else_clause(struct lexer *lexer);

/**
  * \fn struct ast *parser_do_group(struct lexer *lexer, int needed)
  * \brief            parse the rule do_group of the grammar
  * \param lexer      lexer which permits to lex interactively
  * \param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

struct ast *parser_do_group(struct lexer *lexer, int needed);

/**
  * \fn struct ast *parser_case_close(struct lexer *lexer, int needed)
  * \brief            parse the rule case_close of the grammar
  * \param lexer      lexer which permits to lex interactively
  * \param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

struct ast *parser_case_close(struct lexer *lexer, int needed);

/**
  * \fn struct ast *parser_case_item(struct lexer *lexer, int needed)
  * \brief            parse the rule case_item of the grammar
  * \param lexer      lexer which permits to lex interactively
  * \param needed     boolean which state if the lexer has to ask another line
  * \return           the ast
  */

struct ast *parser_case_item(struct lexer *lexer, int needed);

#endif /* ! PARSER_H */
