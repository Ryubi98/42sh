#define _GNU_SOURCE
#include "parser.h"
#include "lexer_utils.h"

static void error(struct lexer *lexer, const char *str)
{
    if (lexer->error == 1)
        return;
    lexer->error_msg = calloc(4096, sizeof(char));
    snprintf(lexer->error_msg, 4096, "%s", str);
    lexer->error = 1;
}

struct ast *parser_input(struct lexer *lexer)
{
    struct ast *ast = parser_list(lexer);
    int result = lexer_peek(lexer, END_OF_FILE, 0);
    if (result)
        return ast;
    result = lexer_peek(lexer, NEW_LINE, 0);
    if (result)
        return ast;
    ast_free(ast);
    return NULL;
}

struct ast *parser_list(struct lexer *lexer)
{
    struct ast *list = calloc(1, sizeof(struct ast));
    list->type = AST_LIST;
    list->list = list_init();
    struct ast *and_or = parser_and_or(lexer, 0);
    if (and_or != NULL)
    {
        list_add(list->list, and_or);
        while (1)
        {
            int sep = lexer_peek(lexer, SEMICOLON, 0);
            if (sep)
                lexer_pop(lexer, SEMICOLON, 0);
            else
                sep = lexer_pop(lexer, BITWISE_AND, 0);
            if (sep)
            {
                and_or = parser_and_or(lexer, 0);
                if (and_or)
                    list_add(list->list, and_or);
                else
                    return list;
            }
            else
                return list;
        }
    }
    ast_free(list);
    return NULL;
}

struct ast *parser_and_or(struct lexer *lexer, int needed)
{
    struct ast *pipeline = parser_pipeline(lexer, needed);
    if (pipeline != NULL)
    {
        int is_and = lexer_peek(lexer, LOGICAL_AND, 0);
        int is_or = 0;
        if (is_and)
            lexer_pop(lexer, LOGICAL_AND, 0);
        else
            is_or = lexer_pop(lexer, LOGICAL_OR, 0);
        if (is_and || is_or)
        {
            struct ast *and_or = calloc(1, sizeof(struct ast));
            and_or->type = AST_OP;
            and_or->body = pipeline;
            and_or->op = is_and ? AND : OR;
            while (lexer_pop(lexer, NEW_LINE, 0))
                continue;
            and_or->else_body = parser_and_or(lexer, 1);
            if (and_or->else_body == NULL)
            {
                error(lexer, "expected right statement");
                ast_free(and_or);
                return NULL;
            }
            return and_or;
        }
        return pipeline;
    }
    if (needed)
        error(lexer, "expected and_or");
    return pipeline;
}

static struct ast *parser_pipeline_rec(struct lexer *lexer, int needed)
{
    struct ast *ast = parser_command(lexer, needed);
    if (ast == NULL)
    {
        if (needed)
            error(lexer, "expected command");
        return NULL;
    }
    if (lexer_pop(lexer, BITWISE_OR, 0))
    {
        while (lexer_pop(lexer, NEW_LINE, 0))
            continue;
        struct ast *cmd2 = parser_pipeline_rec(lexer, 1);
        if (cmd2 == NULL)
        {
            error(lexer, "expected right command");
            ast_free(ast);
            return NULL;
        }
        struct ast *pipe = calloc(1, sizeof(struct ast));
        pipe->type = AST_OP;
        pipe->op = PIPE;
        pipe->body = ast;
        pipe->else_body = cmd2;
        return pipe;
    }
    return ast;
}

struct ast *parser_pipeline(struct lexer *lexer, int needed)
{
    int is_not = lexer_pop(lexer, EM, 0);
    if (is_not)
        needed = 1;
    struct ast *not = NULL;
    struct ast *ast = parser_pipeline_rec(lexer, needed);
    if (ast == NULL)
    {
        if (needed)
            error(lexer, "expected statement");
        return NULL;
    }
    if (is_not)
    {
        not = calloc(1, sizeof(struct ast));
        not->type = AST_OP;
        not->op = NOT;
        not->body = ast;
        ast = not;
    }
    return ast;
}

struct ast *parser_command(struct lexer *lexer, int needed)
{
    struct ast *ast = parser_simple_command(lexer, needed);
    if (ast == NULL)
        ast = parser_shell_command(lexer, needed);
    if (ast == NULL)
        ast = parser_funcdec(lexer, needed);
    if (ast == NULL && needed)
        error(lexer, "expected command");
    return ast;
}


static int check_keyword(struct lexer *lexer, int needed)
{
    return lexer_peek(lexer, IF, needed) || lexer_peek(lexer, ELIF, needed)
    || lexer_peek(lexer, FI, needed) || lexer_peek(lexer, THEN, needed)
    || lexer_peek(lexer, ELSE, needed) || lexer_peek(lexer, WHILE, needed)
    || lexer_peek(lexer, UNTIL, needed) || lexer_peek(lexer, FOR, needed)
    || lexer_peek(lexer, DO, needed) || lexer_peek(lexer, DONE, needed)
    || lexer_peek(lexer, CASE, needed) || lexer_peek(lexer, ESAC, needed)
    || lexer_peek(lexer, IN, needed) || lexer_peek(lexer, EM, needed);
}

struct ast *parser_simple_command(struct lexer *lexer, int needed)
{
    if (check_keyword(lexer, needed || lexer->mode != INTERACTIVE))
        return NULL;
    struct ast *ast = calloc(1, sizeof(struct ast));
    ast->type = AST_ASSIGNMENT;
    ast->assignment = calloc(4096, sizeof(char *));

    int cont = 1;
    while (cont)
    {
        cont = parser_prefix(lexer, 0, ast);
        if (cont == 1)
            ast->arga += 1;
    }
    ast->command = calloc(4096, sizeof(char *));

    cont = 1;
    int n2 = needed && ast->arga == 0;
    while (cont)
    {
        cont = parser_element(lexer, n2, ast);
        if (cont == 1)
            ast->argc += 1;
        n2 = 0;
    }
    if (ast->arga > 0)
    {
        ast->assignment = realloc(ast->assignment,
            (ast->arga + 1) * sizeof(char *));
        if (ast->argc > 0)
            ast->type = AST_COMMAND;
        else
        {
            free(ast->command);
            return ast;
        }
    }
    else
    {
        if (ast->argc == 0)
        {
            if (needed)
                error(lexer, "expected command");
            free(ast->command);
            free(ast->assignment);
            ast_free(ast);
            return NULL;
        }
        free(ast->assignment);
        ast->type = AST_COMMAND;
    }
    ast->command = realloc(ast->command, (ast->argc + 1) * sizeof(char *));
    return ast;
}

struct ast *parser_shell_command(struct lexer *lexer, int needed)
{
    struct ast *ast = parser_rule_if(lexer, needed);
    if (ast == NULL)
        ast = parser_rule_while(lexer, needed);
    if (ast == NULL)
        ast = parser_rule_for(lexer, needed);
    if (ast == NULL)
        ast = parser_rule_case(lexer, needed);
    if (ast == NULL)
        ast = parser_rule_until(lexer, needed);
    if (needed && ast == NULL)
        error(lexer, "expected shell command");
    return ast;
}

struct ast *parser_funcdec(struct lexer *lexer, int needed)
{
    needed = needed;
    lexer = lexer;
    return NULL;
}

int parser_redirection(struct lexer *lexer, int needed, struct ast *ast)
{
    int shift = 0;
    ast->fdin = 0;
    if (!update_buffer(lexer, needed))
        return 0;
    char c = lexer->buffer[lexer->start];
    if (c >= '0' && c <= '9')
    {
        ast->fdin = c - '0';
        lexer->start += ++shift;
    }
    if (get_keyword(lexer, LEFT_CHEVRON))
        ast->redir_type = LEFT_CHEVRON;
    else if (get_keyword(lexer, RIGHT_CHEVRON))
        ast->redir_type = RIGHT_CHEVRON;
    else if (get_keyword(lexer, DOUBLE_LEFT_CHEVRON))
    {
        ast->redir_type = DOUBLE_LEFT_CHEVRON;
        shift++;
        lexer->start += 1;
    }
    else if (get_keyword(lexer, DOUBLE_RIGHT_CHEVRON))
    {
        ast->redir_type = DOUBLE_RIGHT_CHEVRON;
        shift++;
        lexer->start += 1;
    }
    else
    {
        lexer->start -= shift;
        return 0;
    }
    shift++;
    lexer->start += 1;
    while (*(lexer->buffer + lexer->start) == ' '
                || *(lexer->buffer + lexer->start) == '\t')
    {
            lexer->start += 1;
            shift++;
    }
    int r = get_word(lexer, 0);
    if (r)
    {
        if (ast->fdout_word)
            free(ast->fdout_word);
        ast->fdout_word = lexer->word;
        lexer->word = NULL;
        lexer->start += r;
        return 1;
    }
    lexer->start -= shift;
    return 0;
}

int parser_prefix(struct lexer *lexer, int needed, struct ast *ast)
{
    int r = parser_redirection(lexer, needed, ast);
    if (r)
        return 2;
    lexer->word = NULL;
    lexer_pop(lexer, ASSIGNMENT_WORD, needed);
    if (lexer->word == NULL || *(lexer->word) == '\0')
    {
        free(lexer->word);
        lexer->word = NULL;
    }
    if (lexer->word == NULL)
        return 0;
    ast->assignment[ast->arga] = lexer->word;
    return 1;
}

int parser_element(struct lexer *lexer, int needed, struct ast *ast)
{
    int r = parser_redirection(lexer, needed, ast);
    if (r)
        return 2;
    lexer->word = NULL;
    lexer_pop(lexer, WORD, needed);
    if (lexer->word == NULL || *(lexer->word) == '\0')
    {
        free(lexer->word);
        lexer->word = NULL;
    }
    if (lexer->word == NULL)
        return 0;
    ast->command[ast->argc] = lexer->word;
    return 1;
}

struct ast *parser_compound_list(struct lexer *lexer, int needed)
{
    while (lexer_pop(lexer, NEW_LINE, 0))
        continue;
    struct ast *list = calloc(1, sizeof(struct ast));
    list->type = AST_LIST;
    list->list = list_init();
    struct ast *and_or = parser_and_or(lexer, needed);
    if (and_or != NULL)
    {
        list_add(list->list, and_or);
        while (1)
        {
            update_buffer(lexer, 1);
            if (!lexer_pop(lexer, SEMICOLON, 0))
                lexer_pop(lexer, BITWISE_OR, 0);
            while (lexer_pop(lexer, NEW_LINE, 0))
                continue;
            and_or = parser_and_or(lexer, 0);
            if (and_or)
                list_add(list->list, and_or);
            else
                return list;
        }
    }
    if (needed)
        error(lexer, "expected coumpound list");
    ast_free(list);
    return NULL;
}

static char *parser_element_for(struct lexer *lexer, int needed)
{
    lexer->word = NULL;
    lexer_pop(lexer, WORD, needed);
    if (lexer->word == NULL || *(lexer->word) == '\0')
    {
        free(lexer->word);
        lexer->word = NULL;
    }
    return lexer->word;
}


struct ast *parser_rule_for(struct lexer *lexer, int needed)
{
    if (!lexer_pop(lexer, FOR, needed))
    {
        return NULL;
    }
    struct ast *ast = calloc(1, sizeof(struct ast));
    ast->var_name = parser_element_for(lexer, 1);
    if (ast->var_name == NULL)
    {
        ast_free(ast);
        error(lexer, "expected identifier");
        return NULL;
    }
    if (!lexer_pop(lexer, SEMICOLON, 0))
    {
        while (lexer_pop(lexer, NEW_LINE, 0))
            continue;
        if (!lexer_pop(lexer, IN, 1))
        {
            ast_free(ast);
            error(lexer, "expected in");
            return NULL;
        }
        ast->for_list = calloc(4096, sizeof(char *));
        char *word = parser_element_for(lexer, 0);
        while (word != NULL)
        {
            ast->for_list[ast->argf] = word;
            ast->argf += 1;
            word = parser_element_for(lexer, 0);
        }
        if (ast->argf == 0)
        {
            free(ast->for_list);
            ast->for_list = NULL;
        }
        if (!lexer_pop(lexer, SEMICOLON, 1) && !lexer_pop(lexer, NEW_LINE, 1))
        {
            ast_free(ast);
            error(lexer, "expected separator");
            return NULL;
        }
    }
    while (lexer_pop(lexer, NEW_LINE, 0))
        continue;
    ast->body = parser_do_group(lexer, 1);
    if (ast->body == NULL)
    {
        ast_free(ast);
        error(lexer, "expected body");
        return NULL;
    }
    ast->type = AST_FOR;
    return ast;
}

struct ast *parser_rule_while(struct lexer *lexer, int needed)
{
    if (!lexer_pop(lexer, WHILE, needed))
    {
        return NULL;
    }
    struct ast *condition = parser_compound_list(lexer, 1);
    if (condition == NULL)
    {
        error(lexer, "expected condition");
        return NULL;
    }
    struct ast *body = parser_do_group(lexer, 1);
    if (body == NULL)
    {
        ast_free(condition);
        error(lexer, "expected body");
        return NULL;
    }
    struct ast *ast = calloc(1, sizeof(struct ast));
    ast->type = AST_WHILE;
    ast->condition = condition;
    ast->body = body;
    return ast;
}

struct ast *parser_rule_case(struct lexer *lexer, int needed)
{
    needed = needed;
    lexer = lexer;
    return NULL;
}

struct ast *parser_rule_until(struct lexer *lexer, int needed)
{
    if (!lexer_pop(lexer, UNTIL, needed))
    {
        return NULL;
    }
    struct ast *condition = parser_compound_list(lexer, 1);
    if (condition == NULL)
    {
        error(lexer, "expected condition");
        return NULL;
    }
    struct ast *body = parser_do_group(lexer, 1);
    if (body == NULL)
    {
        ast_free(condition);
        error(lexer, "expected body");
        return NULL;
    }
    struct ast *ast = calloc(1, sizeof(struct ast));
    ast->type = AST_UNTIL;
    ast->condition = condition;
    ast->body = body;
    return ast;
}

struct ast *parser_rule_if(struct lexer *lexer, int needed)
{
    if (!lexer_pop(lexer, IF, needed))
    {
        return NULL;
    }
    struct ast *ast1 = parser_compound_list(lexer, 1);
    if (ast1 == NULL)
        error(lexer, "expected condition");
    if (!lexer_pop(lexer, THEN, 1))
    {
        error(lexer, "expected then");
        ast_free(ast1);
        return NULL;
    }
    struct ast *ast2 = parser_compound_list(lexer, 1);
    if (ast2 == NULL)
    {
        error(lexer, "expected body");
        ast_free(ast1);
        return NULL;
    }
    struct ast *else_clause = parser_else_clause(lexer);
    struct ast *ast = calloc(1, sizeof(struct ast));
    ast->type = AST_IF;
    ast->condition = ast1;
    ast->body = ast2;
    ast->else_body = else_clause;
    if (!lexer_pop(lexer, FI, 1))
    {
        error(lexer, "expected fi");
        ast_free(ast);
        return NULL;
    }
    return ast;
}

struct ast *parser_else_clause(struct lexer *lexer)
{
    if (lexer_pop(lexer, ELSE, 1))
    {
        return parser_compound_list(lexer, 1);
    }
    if (lexer_pop(lexer, ELIF, 1))
    {
        struct ast *ast1 = parser_compound_list(lexer, 1);
        if (ast1 == NULL)
            error(lexer, "expected condition");
        if (!lexer_pop(lexer, THEN, 1))
        {
            error(lexer, "expected then");
            ast_free(ast1);
            return NULL;
        }
        struct ast *ast2 = parser_compound_list(lexer, 1);
        if (ast2 == NULL)
        {
            error(lexer, "expected body");
            ast_free(ast1);
            return NULL;
        }
        struct ast *else_clause = parser_else_clause(lexer);
        struct ast *ast = calloc(1, sizeof(struct ast));
        ast->type = AST_IF;
        ast->condition = ast1;
        ast->body = ast2;
        ast->else_body = else_clause;
        return ast;
    }
    return NULL;
}

struct ast *parser_do_group(struct lexer *lexer, int needed)
{
    if (!lexer_pop(lexer, DO, needed))
    {
        error(lexer, "exepected do");
        return NULL;
    }
    struct ast *ast = parser_compound_list(lexer, 1);
    if (ast == NULL)
    {
        ast_free(ast);
        error(lexer, "expected body");
        return NULL;
    }
    if (!lexer_pop(lexer, DONE, 1))
    {
        ast_free(ast);
        error(lexer, "exepected done");
        return NULL;
    }
    return ast;
}

struct ast *parser_case_close(struct lexer *lexer, int needed)
{
    needed = needed;
    lexer = lexer;
    return NULL;
}

struct ast *parser_case_item(struct lexer *lexer, int needed)
{
    needed = needed;
    lexer = lexer;
    return NULL;
}

struct ast *parser(struct lexer *lexer)
{
    if (lexer == NULL)
    {
        fprintf(stderr, "42sh: Error while parsing: lexer is null\n");
        exit(EXIT_FAILURE);
    }
    return parser_input(lexer);
}
