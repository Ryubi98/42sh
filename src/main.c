#define _GNU_SOURCE
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <readline/history.h>
#include <sys/types.h>
#include <pwd.h>

#include "ast.h"
#include "options.h"
#include "lexer.h"
#include "parser.h"
#include "exec.h"
#include "variables.h"
#include "utils.h"

/**
* \file
* \mainpage
*/

static void print_options(struct options *option)
{
    printf("Options:\n");
    printf("--norc = %d\n", option->norc);
    printf("--ast-print = %d\n", option->ast_print);
    printf("--version = %d\n", option->version);
    printf("--option-print = %d\n", option->option_print);
    printf("script_file = %s\n", option->script_file);
    printf("-c = %d --- c_arg = %s\n", option->c, option->c_arg);
    printf("+O = %d\n", option->plus_O);
    printf("-O = %d\n", option->minus_O);
}

static void ast_printer(struct options *options, struct ast *ast)
{
    if (options->ast_print)
    {
        FILE *dot = fopen("ast.dot", "w+");
        ast_print(ast, dot);
        fclose(dot);
    }
}

static int parser_error(struct ast *ast, struct lexer *lexer,
                            int *cont, int line)
{
    if (ast == NULL)
    {
        if (lexer->error)
            fprintf(stderr,
                "42sh: Error at parsing: block %d: %s\n",
                line,
                lexer->error_msg);
        else if (lexer->buffer[0] == '#')
        {
            lexer_free(lexer);
            return -10;
        }
        else
        {
            fprintf(stderr,
                    "42sh: Error at parsing: block %d: %s\n",
                    line,
                    "Syntax error");
        }
        if (lexer->mode != INTERACTIVE)
        {
            if (lexer->mode == ARG)
                exit(1);
            exit(2);
        }
        *cont = 1;
        return 2;
    }
    return 0;
}

static int run_42sh(struct options *option, struct variables **vs,
                    struct variables **alias, int status)
{
    int line = 0;
    int cont = 1;
    do
    {
        declare_status(vs, status);
        struct lexer *lexer = lexer_init(option->c_arg, option, vs);
        while (lexer_pop(lexer, NEW_LINE, 0))
            continue;
        if (lexer->buffer[lexer->start] != 0)
        {
            struct ast *ast = parser(lexer);
            int r = parser_error(ast, lexer, &cont, line);
            if (r == -10)
                break;
            else
                status = r;
            ast_printer(option, ast);
            if (lexer->error == 0)
                status = execute(ast, vs, alias);
            line++;
            declare_status(vs, status);
            ast_free(ast);
        }
        else
            cont = lexer->mode == INTERACTIVE;
        lexer_free(lexer);
    } while (!option->c && cont);
    return status;
}

static int run_42shrc(struct options *options, struct variables **vs,
                        struct variables **alias)
{
    int status = 0;
    if (!options->norc)
    {
        FILE *rc = fopen("/etc/42shrc", "r");
        if (rc != NULL)
        {
            struct options *opt1 = rc_options("/etc/42shrc", rc);
            status = run_42sh(opt1, vs, alias, status);
            free(opt1);
            fclose(rc);
        }
        char *home_dir = secure_getenv("HOME");
        if (home_dir != NULL)
        {
            char *path_dot_rc = calloc(4096, sizeof(char));
            snprintf(path_dot_rc, 4096, "%s/.42shrc", home_dir);
            FILE *dot_rc = fopen(path_dot_rc, "r");
            if (dot_rc != NULL)
            {
                struct options *opt2 = rc_options(path_dot_rc, dot_rc);
                status = run_42sh(opt2, vs, alias, status);
                free(opt2);
                fclose(dot_rc);
            }
            free(path_dot_rc);
        }
    }
    return status;
}

static void free_var_al(struct variables **vs, struct variables **alias)
{
    free_variables(*alias);
    free(alias);
    free_variables(*vs);
    free(vs);
}

/**
* \fn int main(int argc, char *argv[])
* \brief               Main function.
* \param argc          Argument counter.
* \param argv[]        Arguments.
* \return              Return status.
*/

int main(int argc, char *argv[])
{
    struct options *option = calloc(1, sizeof(struct options));
    parse_options(argc, argv, option);
    if (!option->version)
    {
        if (option->option_print)
            print_options(option);
        int status = 0;
        struct variables **vs = calloc(1, sizeof(struct variables *));
        struct variables **alias = calloc(1, sizeof(struct variables *));

        declare_ps1_ps2(vs);
        declare_pwd(vs);

        char *dir = get_path_history();
        read_history(dir);
        free(dir);

        status = run_42shrc(option, vs, alias);
        if (option->script_file != NULL)
            option->script = fopen(option->script_file, "r");

        status = run_42sh(option, vs, alias, status);

        free_var_al(vs, alias);
        if (option->script_file != NULL)
            fclose(option->script);
        free(option);
        exit(status);
    }
    printf("Version 1.0\n");
    exit(EXIT_SUCCESS);
}
