#include <stdio.h>

#include "ast.h"

static void print_array_types(FILE *out, enum AST_TYPE type)
{
    char *types[] =
    {
        "list",
        "cmd",
        "assig",
        "if",
        "while",
        "for",
        "op",
        "case",
        "until"
    };

    fprintf(out, "<%s>", types[type]);
}

static void print_array_operators(FILE *out, enum OPERATOR op)
{
    char *operators[] =
    {
        "OR",
        "AND",
        "GT",
        "GTE",
        "LT",
        "LTE",
        "NOT",
        "EQ",
        "PIPE"
    };

    fprintf(out, "%s", operators[op]);
}

static void print_output(FILE *out, int arg, char **output)
{
    if (arg)
        {
            fprintf(out, "[%s", output[0]);
            for (int i = 1; i < arg; i++)
                fprintf(out, " %s", output[i]);
            fprintf(out, "]");
        }
        else
            fprintf(out, "[null]");
}

void ast_print_node(struct ast *ast, FILE *out)
{
    if (ast == NULL)
    {
        fprintf(out, "(null)");
        return;
    }

    print_array_types(out, ast->type);
    if (ast->type == AST_OP)
        print_array_operators(out, ast->op);
    if (ast->type == AST_COMMAND)
        print_output(out, ast->argc, ast->command);
    if (ast->type == AST_ASSIGNMENT)
        print_output(out, ast->arga, ast->assignment);
    if (ast->type == AST_FOR)
    {
        if (ast->argf)
        {
            fprintf(out, "%s [%s", ast->var_name, ast->for_list[0]);
            for (int i = 1; i < ast->argf; i++)
                fprintf(out, " %s", ast->for_list[i]);
            fprintf(out, "]");
        }
        else
            fprintf(out, "[null]");
    }
}

static void ast_print_rec(struct ast *ast, FILE *out, int father)
{
    static int name = 0;
    name++;
    int c = name;

    if (ast == NULL)
        return;

    fprintf(out, "%d[label=\"", name);
    ast_print_node(ast, out);
    fprintf(out, "\"];");
    if (father)
        fprintf(out, "%d--%d;", father, name);

    if (ast->condition != NULL)
        ast_print_rec(ast->condition, out, c);
    if (ast->body != NULL)
        ast_print_rec(ast->body, out, c);
    if (ast->else_body != NULL)
        ast_print_rec(ast->else_body, out, c);
    for (int i = 0; i < list_size(ast->list); i++)
        ast_print_rec(list_at(ast->list, i), out, c);
}

void ast_print(struct ast *ast, FILE *out)
{
    fprintf(out, "graph graphname { size=\"100,100\";");
    ast_print_rec(ast, out, 0);
    fprintf(out, " }");
}

void ast_free(struct ast *ast)
{
    if (ast == NULL)
        return;
    for (int i = 0; i < ast->argc; i++)
        free(ast->command[i]);
    for (int i = 0; i < ast->arga; i++)
        free(ast->assignment[i]);
    for (int i = 0; i < ast->argf; i++)
        free(ast->for_list[i]);
    if (ast->argf > 0)
        free(ast->for_list);
    if (ast->argc > 0)
        free(ast->command);
    if (ast->arga > 0)
        free(ast->assignment);
    free(ast->fdout_word);
    free(ast->var_name);
    ast_free(ast->condition);
    ast_free(ast->body);
    ast_free(ast->else_body);
    list_free(ast->list);
    free(ast);
}