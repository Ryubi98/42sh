#include <stdio.h>
#include <stdlib.h>

#include "ast_list.h"
#include "ast.h"

struct list *list_init(void)
{
    struct list *list = calloc(1, sizeof(struct list));
    return list;
}

int list_is_empty(struct list *list)
{
    if (list == NULL)
        return 0;
    return list->size == 0;
}

int list_size(struct list *list)
{
    if (list == NULL)
        return 0;
    return list->size;
}