#ifndef AST_H
#define AST_H

#include "ast_list.h"
#include "lexer.h"

/**
  * \enum   AST_TYPE
  * \brief  Enumeration corresponding to the type of an ast.
  */

enum AST_TYPE
{
    AST_LIST,
    AST_COMMAND,
    AST_ASSIGNMENT,
    AST_IF,
    AST_WHILE,
    AST_FOR,
    AST_OP,
    AST_CASE,
    AST_UNTIL
};

/**
  * \enum   OPERATOR
  * \brief  Enumeration corresponding to the different operators.
  */

enum OPERATOR
{
    OR,
    AND,
    GT,
    GTE,
    LT,
    LTE,
    NOT,
    EQ,
    PIPE
};

/**
  * \struct ast
  * \brief  Structure corresponding to the ast of the command.
  */

struct ast
{
    enum AST_TYPE type;

    //IF AST_TYPE == AST_LIST
    struct list *list;

    //IF AST_TYPE == OPERATOR
    enum OPERATOR op;
    int (*operate)(int, int);

    //IF AST_TYPE == AST_IF_WHILE_FOR_OP
    struct ast *condition;
    struct ast *body;
    struct ast *else_body;

    //IF AST_TYPE == AST_FOR
    char *var_name;
    int argf;
    char **for_list;

    //IF AST_TYPE == AST_ASSIGNMENT || AST_COMMAND
    int arga;
    char **assignment;
    //IF AST_TYPE == AST_COMMAND
    int argc;
    char **command;
    //IF REDIR
    int fdin;
    char *fdout_word;
    enum TOKEN redir_type;
};

/**
  * \fn         void ast_print(struct ast *ast, FILE *out)
  * \brief      Generates the file corresponding to the current ast.
  * \param ast  The ast to print.
  * \param out  The file to write the ast.
  */

void ast_print(struct ast *ast, FILE *out);

/**
  * \fn         void ast_print_node(struct ast *ast, FILE *out)
  * \brief      Writes only the current node in the file.
  * \param ast  The node to write.
  * \param out  The file to write the node.
  */

void ast_print_node(struct ast *ast, FILE *out);

/**
  * \fn         void ast_print_node(struct ast *ast, FILE *out)
  * \brief      Frees all the nodes of the ast, and the structure itself.
  * \param ast  The ast to free.
  */

void ast_free(struct ast *ast);

#endif /* ! AST_H */
