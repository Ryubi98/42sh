#ifndef AST_LIST_H
#define AST_LIST_H

#include <stdlib.h>

/**
  * \struct list_node
  * \brief  It is a struct corresponding to a node of the struct list.
  */

struct list_node
{
    struct ast *ast;
    struct list_node *next;
};

/**
  * \struct list
  * \brief  It is a struct for a linked list containing a head and a tail.
  */

struct list
{
    struct list_node *head;
    struct list_node *tail;

    int size;
};

/**
  * \fn     struct list *list_init(void)
  * \brief  It initializes a struct list.
  * \return It returns a new list.
  */

struct list *list_init(void);

/**
  * \fn         int list_is_empty(struct list *list)
  * \brief      Checks if the list in parameter is empty.
  * \param list The list to check.
  * \return     Returns 1 if is is empty, otherwhise 0.
  */

int list_is_empty(struct list *list);

/**
  * \fn         int list_size(struct list *list)
  * \brief      Gives the size of the list.
  * \param list The list to check.
  * \return     Returns the size of the list in parameter.
  */

int list_size(struct list *list);

/**
  * \fn             struct ast *list_at(struct list *list, int index)
  * \brief          Gives the node at index in the list.
  * \param list     The list to check.
  * \param index    The index to check.
  * \return         Returns the node at the index in the list.
  */

struct ast *list_at(struct list *list, int index);

/**
  * \fn         void list_add(struct list *list, struct ast *ast)
  * \brief      Adds a node in the list.
  * \param list The list where the node is added.
  * \param ast  The node to add in the list.
  */

void list_add(struct list *list, struct ast *ast);

/**
  * \fn         void list_free(struct list *list)
  * \brief      Frees the all the nodes in the list as well as the list.
  * \param list The list to free.
  */

void list_free(struct list *list);

#endif /* ! AST_LIST_H */
