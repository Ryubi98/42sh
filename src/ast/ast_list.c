#include <stdlib.h>
#include <stdio.h>

#include "ast_list.h"
#include "ast.h"

struct ast *list_at(struct list *list, int index)
{
    if (list == NULL)
        return NULL;
    if (index >= list->size)
        return NULL;
    struct list_node *node = list->head;
    while (index > 0)
    {
        node = node->next;
        index--;
    }
    return node->ast;
}

void list_add(struct list *list, struct ast *ast)
{
    if (list == NULL)
        return;
    struct list_node *node = calloc(1, sizeof(struct list_node));
    node->ast = ast;
    if (list->head == NULL)
        list->head = node;
    if (list->tail == NULL)
        list->tail = node;
    else
    {
        list->tail->next = node;
        list->tail = node;
    }
    list->size += 1;
}

void list_dump(struct list *list, FILE *out)
{
    fprintf(out, "[");
    struct list_node *node = list->head;
    while (node != NULL)
    {
        ast_print_node(node->ast, out);
        node = node->next;
        if (node != NULL)
            fprintf(out, " | ");
    }
    fprintf(out, "]\n");
}

void list_free(struct list *list)
{
    if (list == NULL)
        return;
    struct list_node *node = list->head;
    while (node != NULL)
    {
        struct list_node *next = node->next;
        ast_free(node->ast);
        free(node);
        node = next;
    }
    free(list);
}