#ifndef BUILTIN_H
#define BUILTIN_H

#include <stdio.h>

/**
  * \fn         int builtin(char **argv, struct variables **vs)
  * \brief      Handles all the builtins.
  * \param argv The string corresponding of the argument.
  * \param vs   A structure contening all the different variables.
  * \return     Returns the status of the builtin.
  */

int builtin(char **argv, struct variables **vs, int fd_out);

/**
  * \fn         size_t argvlen(char **argv)
  * \brief      Gives the number of argument in argv.
  * \param argv The string corresponding of the argument.
  * \return     Returns the size of argv.
  */

size_t argvlen(char **argv);

/**
  * \fn         int builtin_export(char **argv, struct variables **vs)
  * \brief      This function is the builtin export.
  * \param argv The string corresponding of the argument.
  * \param vs   A structure contening all the different variables.
  * \return     Returns the status of the builtin export.
  */

int builtin_export(char **argv, struct variables **vs);

/**
  * \fn             int builtin_alias(char **argv, struct variables **alias)
  * \brief          It is a struct corresponding to a node of the struct list.
  * \param argv     The string corresponding of the argument.
  * \param alias    A structure contening all the different aliases.
  * \return         Returns the status of the builtin alias.
  */

int builtin_alias(char **argv, struct variables **alias);

/**
  * \fn             int builtin_unalias(char **argv, struct variables **alias)
  * \brief          It is a struct corresponding to a node of the struct list.
  * \param argv     The string corresponding to the argument.
  * \param alias    A structure contening all the different aliases.
  * \return         Retruns the status of the builtin unalias.
  */

int builtin_unalias(char **argv, struct variables **alias);

#endif /* ! BUILTIN_H */
