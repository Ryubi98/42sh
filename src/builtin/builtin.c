#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <readline/history.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#include "variables.h"
#include "utils.h"

size_t argvlen(char **argv)
{
    if (argv == NULL)
        return 0;
    int i = 0;
    while (argv[i])
        i++;
    return i;
}

static int builtin_history(char **argv)
{
    if (argvlen(argv) > 1)
    {
        if (argv[1][0] == '-')
        {
            if (strcmp(argv[1], "-c") == 0)
                clear_history();
            else if (strcmp(argv[1], "-r") == 0)
            {
                char *dir = get_path_history();
                clear_history();
                read_history(dir);
                free(dir);
            }
            else
                return 2;
            return 0;
        }
        return 1;
    }
    HISTORY_STATE *hs = history_get_history_state();
    for (int i = 0; i < hs->length; i++)
        printf("%5d  %s\n", i + 1, hs->entries[i]->line);
    return 0;
}

static int builtin_exit(char **argv)
{
    int argc = argvlen(argv);
    if (isatty(STDIN_FILENO))
    {
        char *dir = get_path_history();
        append_history(500, dir);
        history_truncate_file(dir, 500);
        free(dir);
    }
    int status = 0;
    if (argc > 1)
        status = atoi(argv[1]);
    exit(status);
}

static int builtin_cd(char **argv, struct variables **vs)
{
    if (argvlen(argv) > 1 && strncmp(argv[1], "-", 1) == 0)
    {
        const char *oldpwda = get_variable(*vs, "OLDPWD");
        chdir(oldpwda);
        char *oldpwd = calloc(4096, sizeof(char));
        const char *o = get_variable(*vs, "PWD");
        snprintf(oldpwd, 4096, "OLDPWD=%s", o);
        *vs = add_variable(*vs, oldpwd, 0);

        char *pwd = calloc(4096, sizeof(char));
        snprintf(pwd, 5, "PWD=%s", oldpwda);
        getcwd(pwd + 4, 4096 - 4);
        *vs = add_variable(*vs, pwd, 0);
        return 0;
    }
    else if (argvlen(argv) > 1 && chdir(argv[1]) == 0)
    {
        char *oldpwd = calloc(4096, sizeof(char));
        snprintf(oldpwd, 4096, "OLDPWD=%s", get_variable(*vs, "PWD"));
        *vs = add_variable(*vs, oldpwd, 0);
        free(oldpwd);

        char *pwd = calloc(4096, sizeof(char));
        snprintf(pwd, 5, "PWD=");
        getcwd(pwd + 4, 4096 - 4);
        *vs = add_variable(*vs, pwd, 0);
        free(pwd);
        return 0;
    }
    return 1;
}

static int builtin_echo(char **argv, int out)
{
    int is_n = 0;
    int i = 2;
    if (argv[1])
    {
        is_n = strncmp("-n", argv[1], 2) == 0;
        if (!is_n)
            dprintf(out, "%s", argv[1]);
    }
    while (argv[1] && argv[i])
    {
        if (!is_n)
            dprintf(out, " ");
        dprintf(out, "%s", argv[i]);
        i++;
    }
    if (!is_n)
        dprintf(out, "\n");
    return 0;
}

static int builtin_strcmp(char **argv)
{
    return argvlen(argv) != 3 || strcmp(argv[1], argv[2]);
}

int builtin_export(char **argv, struct variables **vs)
{
    int argc = argvlen(argv);
    if (argc == 1)
    {
        dump_variables("declare -x", *vs);
        return 0;
    }
    for (int i = 1; i < argc; i++)
    {
        *vs = add_variable(*vs, argv[i], 1);
    }
    return 0;
}

int builtin_alias(char **argv, struct variables **alias)
{
    int argc = argvlen(argv);
    if (argc == 1)
    {
        dump_variables("alias", *alias);
        return 0;
    }
    for (int i = 1; i < argc; i++)
    {
        *alias = add_variable(*alias, argv[i], 1);
    }
    return 0;
}

int builtin_unalias(char **argv, struct variables **alias)
{
    int argc = argvlen(argv);
    int res = 2;
    if (argc == 1)
    {
        fprintf(stderr, "unalias: usage: unalias [-a] name [name ...]\n");
        return res;
    }
    if (strncmp(argv[1], "-a", 2) == 0)
    {
        remove_all_variables(*alias);
        return 0;
    }
    for (int i = 1; i < argc; i++)
    {
        res = remove_variable(*alias, argv[i]);
    }
    return res;
}

int builtin(char **argv, struct variables **vs, int fd_out)
{
    if (strcmp(argv[0], "echo") == 0)
        return builtin_echo(argv, fd_out);
    else if (strcmp(argv[0], "strcmp") == 0)
        return builtin_strcmp(argv);
    else if (strcmp(argv[0], "cd") == 0)
        return builtin_cd(argv, vs);
    else if (strcmp(argv[0], "exit") == 0)
        return builtin_exit(argv);
    else if (strcmp(argv[0], "history") == 0)
        return builtin_history(argv);
    return -1;
}