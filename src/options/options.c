#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>

#include "options.h"

static void compute_one_long_option(const char *str, struct options *options)
{
    if (strcmp(str, "version") == 0)
        options->version = 1;
    else if (strcmp(str, "ast-print") == 0)
        options->ast_print = 1;
    else if (strcmp(str, "norc") == 0)
        options->norc = 1;
    else if (strcmp(str, "variables-print") == 0)
        options->variables_print = 1;
    else if (strcmp(str, "option-print") == 0)
        options->option_print = 1;
}

static void compute_one_option(int c, const struct option long_options[],
        int option_index, struct options *options)
{
    switch (c)
    {
    case 0:
        compute_one_long_option(long_options[option_index].name, options);
        break;
    case 'c':
        if (options->c)
            break;
        options->c = 1;
        options->c_arg = optarg;
        break;
    case 'O':
        options->minus_O = 1;
        break;
    case '?':
        exit(2);
    default:
        fprintf(stderr, "42sh: Error while parsing options: ");
        fprintf(stderr, "getopt returned character code 0%o\n", c);
        exit(EXIT_FAILURE);
    }
}

static void scan_options_error(char *argv[])
{
    char str[4096] =
    {
        0
    };
    if (!(*(argv[optind]) == '/' || *(argv[optind]) == '.'))
        snprintf(str, 4096, "/usr/bin/%s", argv[optind]);
    else
        snprintf(str, 4096, "%s", argv[optind]);
    if (access(str, F_OK) == 0)
    {
        fprintf(stderr,
            "/usr/bin/%s: /usr/bin/%s : %s\n",
            argv[optind],
            argv[optind],
            "cannot execute binary file");
        exit(126);
    }
    else
    {
        fprintf(stderr,
            "42sh: %s: No such file or directory\n",
            argv[optind]);
        exit(127);
    }
}

static void scan_options(int argc, char *argv[], struct options *options,
        const struct option long_options[])
{
    while (1)
    {
        int option_index = 0;
        int c = getopt_long(argc, argv, "c:O", long_options, &option_index);
        if (c == -1)
            break;
        compute_one_option(c, long_options, option_index, options);
    }

    if (optind < argc && !options->version)
    {
        int is_first = 1;
        while (optind < argc)
        {
            if (strcmp(argv[optind], "+O") == 0)
                options->plus_O = 1;
            else
            {
                if (access(argv[optind], F_OK) == 0)
                {
                    if (is_first)
                        options->script_file = argv[optind];
                    is_first = 0;
                }
                else
                    scan_options_error(argv);
            }
            optind++;
        }
    }
}

void parse_options(int argc, char *argv[], struct options *options)
{
    struct option long_options[] =
    {
        {
            "norc",
            no_argument,
            0,
            0
        },
        {
            "ast-print",
            no_argument,
            0,
            0
        },
        {
            "version",
            no_argument,
            0,
            0
        },
        {
            "option-print",
            no_argument,
            0,
            0
        },
        {
            0
        }
    };
    scan_options(argc, argv, options, long_options);
}

struct options *rc_options(char *script_file, FILE *script)
{
    struct options *rc = calloc(1, sizeof(struct options));
    if (rc == NULL)
        return NULL;

    rc->script_file = script_file;
    rc->script = script;
    return rc;
}