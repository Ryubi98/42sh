#ifndef OPTIONS_H
#define OPTIONS_H

#include <stdio.h>

/**
 * \struct options
 * \brief  Structure corresponding to the options.
 */

struct options
{
    int norc;
    int ast_print;
    int version;
    int c;
    char *c_arg;
    char *script_file;
    FILE *script;
    int plus_O;
    int minus_O;
    int variables_print;
    int option_print;
};

/**
  * \fn             void parse_options(int argc, char *argv[],
  *                                     struct options *options)
  * \brief          Parse all options present in argv.
  * \param argc     Number of parameters in argv.
  * \param argv[]   Arguments.
  * \param options  The struct options where all options are.
  */

void parse_options(int argc, char *argv[], struct options *options);

/**
  * \fn                 struct options *rc_options(char *script_file,
  *                                                 FILE *script)
  * \brief              Add script_file and script to struct options.
  * \param script_file  The script_file.
  * \param script       The script.
  * \return             Return a new struct options.
  */

struct options *rc_options(char *script_file, FILE *script);

#endif /* ! OPTIONS_H */