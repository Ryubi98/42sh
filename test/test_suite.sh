#!/bin/sh
cd test
python -m venv env
source env/bin/activate
pip install --upgrade pip > trash
pip install -r requirements.txt > trash
pytest --verbose --tb=long --disable-pytest-warnings -p no:warnings
deactivate
rm -rf env
rm -rf __pycache__
rm trash
