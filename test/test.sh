#!/bin/bash

cd test
list='and as_argument as_script cat echo for if lexer_tricks ls or until version while'

if [ $# -eq 0 ]; then
    ./run_faster.sh;
else
    if [ $# -eq 1 ]; then
        if [ $1 == "-l" ] || [ $1 == "--list" ]; then
            echo and; echo as_argument; echo as_script;
            echo cat; echo echo; echo for;
            echo if; echo lexer_tricks; echo ls; echo or;
            echo until; echo version; echo while;
        elif [ $1 == "-s" ] || [ $1 == "--sanity" ]; then
            cp test_valgrind.py save.py
            mv conftest.py test_valgrind.py
            mv save.py conftest.py
            ./run_faster.sh
            cp conftest.py save.py
            mv test_valgrind.py conftest.py
            mv save.py  test_valgrind.py        
        elif [ $1 == "-h" ] || [ $1 == "--help" ]; then
            echo '-c <category> or --category <category> : run the test suite for the <category> only';
            echo '-h or --help : display this help';
            echo '-l or --list : show you all categories';
            echo '-s or --sanity : run test suite with valgrind';
        elif [ $1 == "-c" ] || [ $1 == "--category" ]; then
            echo 'Missing argument -c <category> or --category <category>';
        else
            echo Wrong parameter;
        fi;
    elif [ $# -eq 2 ]; then
        if [ $1 == "-c" ] || [ $1 == "--category" ]; then
            for element in $list; do
                if [ $2 != $element ]; then
                    for i in Yml/$element/*.yml; do 
                        j=`echo $i|sed 's/yml$/YML/'`
                        mv $i $j
                    done;
                fi;
            done;
            ./run_faster.sh;
            for element in $list; do
                if [ $2 != $element ]; then
                    for i in Yml/$element/*.YML; do 
                        j=`echo $i|sed 's/YML$/yml/'`
                        mv $i $j
                    done;
                fi;
            done;
        else
            echo 'Wrong utilisation of test, see ./test.sh (-h or --help)';
        fi;
    else
        echo 'Wrong utilisation of test, see ./test.sh (-h or --help)';
    fi;
fi;
