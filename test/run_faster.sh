#!/bin/sh
if [[ "$PWD" == *test ]]; then
    true
else
    cd test
fi
source env/bin/activate
pytest --verbose --tb=long --disable-pytest-warnings -p no:warnings
deactivate
