# content of conftest.py
import pytest
import subprocess
import shlex

def pytest_collect_file(parent, path):
    if path.ext == ".yml":
        return YamlFile(path, parent)

class YamlFile(pytest.File):
    def collect(self):
        import yaml

        raw = yaml.safe_load(self.fspath.open())
        for name, spec in raw.items():
            if spec['type'] == "command_diff":
                yield CommandDiffItem(name, self, spec)
            elif spec['type'] == "output_diff":
                yield OutputDiffItem(name, self, spec)
            else:
                yield ExternalFileItem(name, self, spec)

class YamlItem(pytest.Item):
    def __init__(self, name, parent, spec):
        super(YamlItem, self).__init__(name, parent)
        self.spec = spec
        self.command = str.encode(self.spec['command'])
        if 'expected' in self.spec:
            self.expected = self.spec['expected']

    def runtest(self):
        args = self.command.decode("utf-8")
        arg = shlex.split(args)
        if len(arg) == 1:
            process = subprocess.Popen(["../build/42sh", arg[0]],
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE)
        else:
            process = subprocess.Popen(["../build/42sh", arg[0], arg[1]],
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE)
        out, err = process.communicate()
        process.kill()

        if 'stdout' in self.expected:
            if self.expected['stdout'] != out:
                raise YamlException('stdout', self.expected['stdout'], out)
        elif out:
            raise YamlException('stdout', b'(empty)', out)

        if not('stderr' in self.expected) and err:
            raise YamlException('stderr', b'(empty)', err)

        if 'rvalue' in self.expected:
            if process.returncode != self.expected['rvalue']:
                raise YamlException('return value',
                                    str(self.expected['rvalue']),
                                    str(process.returncode))

    def repr_failure(self, excinfo):
        """ called when self.runtest() raises an exception. """
        if isinstance(excinfo.value, YamlException):
            instance = excinfo.value
            return "\n".join(
                [
                    "usecase execution failed",
                    "%s : expected '%s', got '%s'" % (instance.expected_type,
                                                      instance.exepected_value,
                                                      instance.output_value)
                ]
            )

    def reportinfo(self):
        return self.fspath, 0, "usecase: %s" % self.name

class ExternalFileItem(YamlItem):
    def __init__(self, name, parent, spec):
        super().__init__(name, parent, spec)
        for item in self.expected:
            if item == "rvalue":
                continue
            f = open(self.expected[item], "r")
            self.expected[item] = str.encode(f.read())

class OutputDiffItem(YamlItem):
    def __init__(self, name, parent, spec):
        super().__init__(name, parent, spec)
        for item in self.expected:
            if item == "rvalue":
                self.expected["rvalue"] = int(self.expected["rvalue"])
                continue
            self.expected[item] = str.encode(self.expected[item])
           
class CommandDiffItem(YamlItem):
    def __init__(self, name, parent, spec):
        super().__init__(name, parent, spec)
        args = self.command.decode("utf-8")
        arg = shlex.split(args)
        if len(arg) == 1:
            bash_process = subprocess.Popen(["bash", arg[0]],
                                            stdout=subprocess.PIPE,
                                            stderr=subprocess.PIPE)
        else:
            bash_process = subprocess.Popen(["bash", arg[0], arg[1]],
                                            stdout=subprocess.PIPE,
                                            stderr=subprocess.PIPE)
        out, err = bash_process.communicate()
        self.expected = {}
        if out:
            self.expected['stdout'] = out
        if err:
            self.expected['stderr'] = err
        self.expected['rvalue'] = bash_process.returncode

class YamlException(Exception):
    def __init__(self, expected_type, exepected_value, output_value):
        self.expected_type = expected_type
        self.exepected_value = exepected_value
        self.output_value = output_value
    """ custom exception for error reporting. """
