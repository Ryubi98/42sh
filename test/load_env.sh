#!/bin/sh
cd test
python -m venv env
source env/bin/activate > trash
pip install --upgrade pip > trash
pip install -r requirements.txt > trash
deactivate
rm trash